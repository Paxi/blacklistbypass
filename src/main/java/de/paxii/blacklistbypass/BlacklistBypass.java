package de.paxii.blacklistbypass;

import de.paxii.blacklistbypass.path.PathDetector;
import de.paxii.blacklistbypass.versions.VersionParser;

import javax.swing.*;

/**
 * Created by Lars on 19.05.2016.
 */
public class BlacklistBypass {
	public static final String BASE_URL = "http://paxii.de/BlacklistBypass/";

	public static void main(String[] args) {
		BlacklistBypassFrame bypassFrame = new BlacklistBypassFrame();
		VersionParser versionParser = new VersionParser();

		JFrame frame = new JFrame("BlacklistBypass by Paxi");
		frame.setContentPane(bypassFrame.getPanel());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();

		bypassFrame.getMinecraftPath().setText(PathDetector.detectPath());
		versionParser.getVersionList().getVersionList().forEach((version) -> bypassFrame.getVersionSelection().addItem(version));

		frame.setVisible(true);
	}
}
