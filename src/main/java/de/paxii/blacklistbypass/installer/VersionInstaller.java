package de.paxii.blacklistbypass.installer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.paxii.blacklistbypass.launcher.LauncherProfile;
import de.paxii.blacklistbypass.launcher.LauncherProfileParser;
import de.paxii.blacklistbypass.launcher.LauncherProfiles;
import de.paxii.blacklistbypass.versions.Version;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by Lars on 19.05.2016.
 */
public class VersionInstaller {

	public static void installVersion(String minecraftPath, Version version) {
		if (!minecraftPath.endsWith(File.separator)) {
			minecraftPath += File.separator;
		}
		File destinationFile = new File(
				minecraftPath +
						"versions" +
						File.separator +
						version.getVersion() +
						File.separator +
						version.getVersion() +
						".json"
		);

		System.out.println("DESTINATION PATH: " + destinationFile.getAbsolutePath());

		destinationFile.mkdirs();

		try {
			Files.copy(version.getDownloadUrl().openStream(), Paths.get(destinationFile.toURI()), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			File launcherProfileFile = new File(minecraftPath, "launcher_profiles.json");
			File launcherProfileFileBackup = new File(minecraftPath, "launcher_profiles_backup.json");
			try {
				Files.copy(Paths.get(launcherProfileFile.toURI()), Paths.get(launcherProfileFileBackup.toURI()));
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Launcher should be at " + launcherProfileFile.getAbsolutePath());

				LauncherProfileParser profileParser = new LauncherProfileParser();
				LauncherProfiles launcherProfiles = profileParser.getLauncherProfiles(launcherProfileFile);

				launcherProfiles.getProfiles().forEach((profileName, profile) -> {
					System.out.println("profileName: " + profileName);
					System.out.println("\tname: " + profile.getName());
					System.out.println("\tlastVersionId: " + profile.getLastVersionId());
					System.out.println("\tjavaArgs: " + profile.getJavaArgs());
					System.out.println("\tuseHopperCrashService: " + profile.isUseHopperCrashService());
					System.out.println("\tallowedReleaseTypes: ");
					profile.getAllowedReleaseTypes().forEach((releaseType) -> System.out.println("\t\t" + releaseType));
				});

				System.out.println("selectedProfile: " + launcherProfiles.getSelectedProfile());
				System.out.println("clientToken: " + launcherProfiles.getClientToken());

				System.out.println("authenTicationDatabase: ");
				launcherProfiles.getAuthenticationDatabase().forEach(((uuid, authentication) -> {
					System.out.println(uuid + ":");
					System.out.println("\tdisplayName: " + authentication.getDisplayName());
					System.out.println("\taccessToken: " + authentication.getAccessToken());
					System.out.println("\tuserid: " + authentication.getUserid());
					System.out.println("\tuuid: " + authentication.getUuid());
					System.out.println("\tusername: " + authentication.getUsername());
				}));

				System.out.println("selectedUser: " + launcherProfiles.getSelectedUser());
				System.out.println("launcherVersion:");
				System.out.println("\tname: " + launcherProfiles.getLauncherVersion().getName());
				System.out.println("\tformat: " + launcherProfiles.getLauncherVersion().getFormat());

				LauncherProfile newProfile = new LauncherProfile();

				newProfile.setName(version.getVersion());
				newProfile.setLastVersionId(version.getVersion());
				newProfile.setJavaArgs("-Xmx1G");

				launcherProfiles.getProfiles().put(version.getVersion(), newProfile);
				launcherProfiles.setSelectedProfile(version.getVersion());

				Gson gson = new GsonBuilder().setPrettyPrinting().create();

				String newProfileFileContents = gson.toJson(launcherProfiles);

				try {
					PrintWriter printWriter = new PrintWriter(new FileOutputStream(launcherProfileFile, false));

					printWriter.print(newProfileFileContents);

					printWriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					JOptionPane.showMessageDialog(null, version.getVersion() + " for " + version.getMinecraftVersion() + " should now be installed!");
				}
			}
		}
	}
}
