package de.paxii.blacklistbypass.versions;

import de.paxii.blacklistbypass.BlacklistBypass;
import lombok.Setter;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Lars on 19.05.2016.
 */
public class Version {
	@Setter
	private String version;

	public Version(String version) {
		this.version = version;
	}

	public String getMinecraftVersion() {
		return this.version;
	}

	public String getVersion() {
		return this.version + "NoNetty";
	}

	public URL getDownloadUrl() {
		try {
			return new URL(String.format(BlacklistBypass.BASE_URL + "versions/%s.json", this.getVersion()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String toString() {
		return this.getMinecraftVersion();
	}
}
