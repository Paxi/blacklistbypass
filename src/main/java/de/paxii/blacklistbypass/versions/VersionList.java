package de.paxii.blacklistbypass.versions;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Lars on 19.05.2016.
 */
public class VersionList {
	@Getter
	@Setter
	private ArrayList<Version> versionList;

	public VersionList(ArrayList<Version> versionList) {
		this.versionList = versionList;
	}
}
