package de.paxii.blacklistbypass.versions;

import com.google.gson.Gson;
import de.paxii.blacklistbypass.BlacklistBypass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Lars on 19.05.2016.
 */
public class VersionParser {

	public VersionList getVersionList() {
		String contents = this.getContents();

		if (contents != null) {
			Gson gson = new Gson();
			VersionList versionList = gson.fromJson(contents, VersionList.class);

			if (versionList != null) {
				return versionList;
			}
		}

		return null;
	}

	private String getContents() {
		try {
			URL versionUrl = new URL(BlacklistBypass.BASE_URL + "versions.json");
			BufferedReader br = new BufferedReader(new InputStreamReader(versionUrl.openStream()));
			StringBuilder lines = new StringBuilder();
			String line;

			while ((line = br.readLine()) != null) {
				lines.append(line);
			}

			return lines.toString().trim();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
