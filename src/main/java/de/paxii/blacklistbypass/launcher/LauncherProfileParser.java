package de.paxii.blacklistbypass.launcher;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Lars on 19.05.2016.
 */
public class LauncherProfileParser {

	public LauncherProfiles getLauncherProfiles(File launcherProfileFile) {
		String contents = this.getContents(launcherProfileFile);

		if (contents.length() > 0) {
			Gson gson = new Gson();

			LauncherProfiles launcherProfiles = gson.fromJson(contents, LauncherProfiles.class);

			if (launcherProfiles != null) {
				return launcherProfiles;
			}
		}

		return null;
	}

	private String getContents(File launcherProfileFile) {
		StringBuilder stringBuilder = new StringBuilder();

		try {
			Scanner scanner = new Scanner(launcherProfileFile);

			while (scanner.hasNextLine()) {
				stringBuilder.append(scanner.nextLine());
			}

			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return stringBuilder.toString().trim();
	}
}
