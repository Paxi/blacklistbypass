package de.paxii.blacklistbypass.launcher;

import lombok.Data;

import java.util.HashMap;

/**
 * Created by Lars on 19.05.2016.
 */
@Data
public class LauncherProfiles {
	private HashMap<String, LauncherProfile> profiles;
	private String selectedProfile;
	private String clientToken;
	private HashMap<String, Authentication> authenticationDatabase;
	private String selectedUser;
	private LauncherVersion launcherVersion;
}
