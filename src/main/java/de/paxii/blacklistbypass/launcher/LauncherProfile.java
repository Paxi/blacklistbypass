package de.paxii.blacklistbypass.launcher;

import lombok.Data;

import java.util.ArrayList;

/**
 * Created by Lars on 19.05.2016.
 */
@Data
public class LauncherProfile {
	private String name;
	private String lastVersionId;
	private String javaArgs;
	private boolean useHopperCrashService;
	private ArrayList<String> allowedReleaseTypes = new ArrayList<>();
}
