package de.paxii.blacklistbypass.launcher;

import lombok.Data;

/**
 * Created by Lars on 19.05.2016.
 */
@Data
public class Authentication {
	private String displayName;
	private String accessToken;
	private String userid;
	private String uuid;
	private String username;
}
