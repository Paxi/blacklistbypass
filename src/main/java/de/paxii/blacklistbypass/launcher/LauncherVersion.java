package de.paxii.blacklistbypass.launcher;

import lombok.Data;

/**
 * Created by Lars on 19.05.2016.
 */
@Data
public class LauncherVersion {
	private String name;
	private int format;
}
