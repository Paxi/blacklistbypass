package de.paxii.blacklistbypass.path;

import java.io.File;

/**
 * Created by Lars on 20.05.2016.
 */
public class PathDetector {
	public static String detectPath() {
		String path = "";
		String osName = System.getProperty("os.name").toLowerCase();
		String userHome = System.getProperty("user.home");

		boolean isWindows = osName.contains("win");
		boolean isMac = osName.contains("mac");
		boolean isUnix = osName.contains("nix") || osName.contains("nux");

		if (isWindows) {
			path = System.getenv("APPDATA") + File.separator + ".minecraft" + File.separator;
		} else if (isMac) {
			path = userHome + File.separator + "Application Support" + File.separator + "minecraft" + File.separator;
		} else if (isUnix) {
			path = userHome + File.separator + "minecraft" + File.separator;
		}

		return path;
	}
}
