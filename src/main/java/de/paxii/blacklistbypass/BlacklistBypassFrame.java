package de.paxii.blacklistbypass;

import de.paxii.blacklistbypass.installer.VersionInstaller;
import de.paxii.blacklistbypass.versions.Version;

import javax.swing.*;

/**
 * Created by Lars on 19.05.2016.
 */
public class BlacklistBypassFrame {
	private JPanel panel;
	private JTextField minecraftPath;
	private JComboBox<Version> versionSelection;
	private JButton installButton;

	public BlacklistBypassFrame() {
		installButton.addActionListener((actionEvent) -> {
			VersionInstaller.installVersion(minecraftPath.getText(), (Version) versionSelection.getSelectedItem());
		});
	}

	public JPanel getPanel() {
		return panel;
	}

	public JComboBox<Version> getVersionSelection() {
		return versionSelection;
	}

	public JTextField getMinecraftPath() {
		return minecraftPath;
	}
}
